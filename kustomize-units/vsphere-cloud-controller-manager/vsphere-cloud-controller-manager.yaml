apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    component: cloud-controller-manager
    vsphere-cpi-infra: service-account
  name: cloud-controller-manager
  namespace: kube-system
---
apiVersion: v1
kind: Secret
metadata:
  labels:
    component: cloud-controller-manager
    vsphere-cpi-infra: secret
  name: cloud-provider-vsphere-credentials
  namespace: kube-system
stringData:
  ${SERVER}.password: ${PASSWORD}
  ${SERVER}.username: ${USERNAME}
type: Opaque
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    component: cloud-controller-manager
    vsphere-cpi-infra: role
  name: system:cloud-controller-manager
rules:
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
  - patch
  - update
- apiGroups:
  - ""
  resources:
  - nodes
  verbs:
  - '*'
- apiGroups:
  - ""
  resources:
  - nodes/status
  verbs:
  - patch
- apiGroups:
  - ""
  resources:
  - services
  verbs:
  - list
  - patch
  - update
  - watch
- apiGroups:
  - ""
  resources:
  - services/status
  verbs:
  - patch
- apiGroups:
  - ""
  resources:
  - serviceaccounts
  verbs:
  - create
  - get
  - list
  - watch
  - update
- apiGroups:
  - ""
  resources:
  - persistentvolumes
  verbs:
  - get
  - list
  - watch
  - update
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - create
  - get
  - list
  - watch
  - update
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - coordination.k8s.io
  resources:
  - leases
  verbs:
  - get
  - watch
  - list
  - update
  - create
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    component: cloud-controller-manager
    vsphere-cpi-infra: cluster-role-binding
  name: system:cloud-controller-manager
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:cloud-controller-manager
subjects:
- kind: ServiceAccount
  name: cloud-controller-manager
  namespace: kube-system
- kind: User
  name: cloud-controller-manager
---
apiVersion: v1
data:
  vsphere.conf: |
    global:
      port: 443
      secretName: cloud-provider-vsphere-credentials
      secretNamespace: kube-system
      thumbprint: ${TLS_THUMBPRINT}
    vcenter:
      ${SERVER}:
        datacenters:
        - ${DATACENTER}
        server: ${SERVER}
kind: ConfigMap
metadata:
  name: vsphere-cloud-config
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    component: cloud-controller-manager
    vsphere-cpi-infra: role-binding
  name: servicecatalog.k8s.io:apiserver-authentication-reader
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: extension-apiserver-authentication-reader
subjects:
- kind: ServiceAccount
  name: cloud-controller-manager
  namespace: kube-system
- kind: User
  name: cloud-controller-manager
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    component: cloud-controller-manager
    tier: control-plane
  name: vsphere-cloud-controller-manager
  namespace: kube-system
spec:
  selector:
    matchLabels:
      name: vsphere-cloud-controller-manager
  template:
    metadata:
      labels:
        component: cloud-controller-manager
        name: vsphere-cloud-controller-manager
        tier: control-plane
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.kubernetes.io/control-plane
                operator: Exists
            - matchExpressions:
              - key: node-role.kubernetes.io/master
                operator: Exists
      containers:
      - args:
        - --v=2
        - --cloud-provider=vsphere
        - --cloud-config=/etc/cloud/vsphere.conf
        image: gcr.io/cloud-provider-vsphere/cpi/release/manager:v1.24.4
        name: vsphere-cloud-controller-manager
        resources:
          requests:
            cpu: 200m
        volumeMounts:
        - mountPath: /etc/cloud
          name: vsphere-config-volume
          readOnly: true
      hostNetwork: true
      priorityClassName: system-node-critical
      securityContext:
        runAsUser: 1001
      serviceAccountName: cloud-controller-manager
      tolerations:
      - effect: NoSchedule
        key: node.cloudprovider.kubernetes.io/uninitialized
        value: "true"
      - effect: NoSchedule
        key: node-role.kubernetes.io/master
        operator: Exists
      - effect: NoSchedule
        key: node-role.kubernetes.io/control-plane
        operator: Exists
      - effect: NoSchedule
        key: node.kubernetes.io/not-ready
        operator: Exists
      volumes:
      - configMap:
          name: vsphere-cloud-config
        name: vsphere-config-volume
  updateStrategy:
    type: RollingUpdate
